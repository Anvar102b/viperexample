//
//  NetworkOperation.m
//  ViperExample
//
//  Created by Anvar Basharov on 27.01.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "NetworkOperation.h"

@interface NetworkOperation ()

@property (nonatomic, strong) NSURLSessionDataTask* task;
@property (nonatomic, strong) NSURLSession* session;
@property (nonatomic, strong) NSURLRequest* request;

@end

@implementation NetworkOperation

- (instancetype)initWithSession:(NSURLSession *)session request:(NSURLRequest *)request {
    self = [super init];
    if (self) {
        self.session = session;
        self.request = request;
    }
    return self;
}

- (void)main {
    if (self.cancelled) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    self.task = [self.session dataTaskWithRequest:self.request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!weakSelf.cancelled && weakSelf.delegate) {
            [weakSelf.delegate didLoadData:data response:response error:error];
        }
        [weakSelf complete];
    }];
    [self.task resume];
}

- (void)cancel {
    [super cancel];
    [self.task cancel];
}

@end
