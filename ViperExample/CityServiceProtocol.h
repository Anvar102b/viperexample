//
//  CityServiceProtocol.h
//  ViperExample
//
//  Created by Anvar Basharov on 02.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetworkError;

@protocol CityServiceProtocol <NSObject>

- (void)getCities:(void(^)(NSArray* cities))success
          failure:(void(^)(NetworkError* error))failure;

@end
