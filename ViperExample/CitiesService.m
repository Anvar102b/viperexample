//
//  CitiesService.m
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "CitiesService.h"
#import "CompoundNetworkOperation.h"

#import "City.h"
#import "NetworkError.h"

#import "OperationSchedulerProtocol.h"

@implementation CitiesService

- (void)getCities:(void(^)(NSArray* cities))success
          failure:(void(^)(NetworkError* error))failure {
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self citiesUrl]]];
    CompoundNetworkOperation* operation = [[CompoundNetworkOperation alloc] initWithSession:[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]] request:request completion:^(id response, NetworkError *error) {
        if (response) {
            NSArray* cities = response[@"Offices"];
            NSMutableArray* muteArray = [NSMutableArray array];
            for (NSDictionary* dict in cities) {
                City* city = [[City alloc] initWithResponse:dict];
                [muteArray addObject:city];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) {
                    success(muteArray);
                }
            });
        }
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (failure) {
                    failure(error);
                }
            });
        }
    }];
    [self.operationScheduler addOperation:operation];
}

- (NSString*)citiesUrl {
    return @"http://modulbank.ru/api/cities/defaults";
}

@end
