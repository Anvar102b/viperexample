//
//  CompoundNetworkOperation.m
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "CompoundNetworkOperation.h"
#import "NetworkOperation.h"
#import "SerializationOperation.h"
#import "NetworkError.h"

@interface CompoundNetworkOperation () <SerializationOperationDelegate>

@property (nonatomic, copy) void (^block)(id response, NetworkError* error);
@property (nonatomic, strong) NSURLSession* session;
@property (nonatomic, strong) NSOperationQueue* internalQueue;

@end

@implementation CompoundNetworkOperation

- (instancetype)initWithSession:(NSURLSession*)session request:(NSURLRequest*)request completion:(void(^)(id response, NetworkError* error))completion
{
    self = [super init];
    if (self) {
        self.internalQueue = [[NSOperationQueue alloc] init];
        self.request = request;
        self.session = session;
        self.block = ^void(id response, NetworkError* error) {
            if (completion) {
                completion(response, error);
            }
        };
    }
    return self;
}

- (void)main {
    if (self.isCancelled || !self.request || !self.session) {
        [self.internalQueue cancelAllOperations];
        return;
    }
    NetworkOperation* networkOperation = [[NetworkOperation alloc] initWithSession:self.session request:self.request];
    SerializationOperation* serializationOperation = [[SerializationOperation alloc] initWithData:nil response:nil error:nil];
    networkOperation.delegate = serializationOperation;
    serializationOperation.delegate = self;
    [serializationOperation addDependency:networkOperation];
    [self.internalQueue addOperations:@[networkOperation, serializationOperation] waitUntilFinished:NO];
}

- (void)cancel {
    [super cancel];
    [self.internalQueue cancelAllOperations];
}

#pragma mark - SerializationOperationDelegate

- (void)didSerialize:(id)response error:(NetworkError*)error {
    if (self.cancelled) {
        return;
    }
    self.block(response, error);
    [self complete];
}

@end
