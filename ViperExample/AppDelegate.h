//
//  AppDelegate.h
//  ViperExample
//
//  Created by Anvar Basharov on 27.01.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

