//
//  NetworkError.m
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "NetworkError.h"

@implementation NetworkError

- (instancetype)initWithText:(NSString*)text {
    self = [super init];
    if (self) {
        self.text = text;
    }
    return self;
}

@end
