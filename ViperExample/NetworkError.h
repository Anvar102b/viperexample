//
//  NetworkError.h
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkError : NSObject

@property (nonatomic, strong) NSString* text;
@property (nonatomic, assign) NSInteger code;

- (instancetype)initWithText:(NSString*)text;

@end
