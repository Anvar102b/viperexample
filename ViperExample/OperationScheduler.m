//
//  OperationScheduler.m
//  ViperExample
//
//  Created by Anvar Basharov on 02.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "OperationScheduler.h"

@interface OperationScheduler ()

@property (nonatomic, strong) NSOperationQueue* queue;

@end

@implementation OperationScheduler

- (instancetype)init
{
    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount;
    }
    return self;
}

#pragma mark - OperationSchedulerProtocol

- (void)addOperation:(NSOperation *)operation {
    [self.queue addOperation:operation];
}

- (void)cancelAllOperations {
    [self.queue cancelAllOperations];
}

@end
