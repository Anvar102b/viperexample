//
//  SerializationOperation.h
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "AsyncOperation.h"
#import "NetworkOperation.h"

@class NetworkError;

@protocol SerializationOperationDelegate <NSObject>

- (void)didSerialize:(id)response error:(NetworkError*)error;

@end

@interface SerializationOperation : AsyncOperation <NetworkOperationDelegate>

@property (nonatomic, weak) id <SerializationOperationDelegate> delegate;

- (instancetype)initWithData:(NSData*)data response:(NSURLResponse*)response error:(NSError*)error;

@end
