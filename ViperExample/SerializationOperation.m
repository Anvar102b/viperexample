//
//  SerializationOperation.m
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "SerializationOperation.h"
#import "NetworkError.h"

@interface SerializationOperation () 

@property (nonatomic, strong) NSData* data;
@property (nonatomic, strong) NSURLResponse* response;
@property (nonatomic, strong) NSError* error;

@end

@implementation SerializationOperation

- (instancetype)initWithData:(NSData*)data response:(NSURLResponse*)response error:(NSError*)error
{
    self = [super init];
    if (self) {
        self.data = data;
        self.response = response;
        self.error = error;
    }
    return self;
}

- (void)main {
    if (self.cancelled) {
        return;
    }
    __weak __typeof(self)weakSelf = self;
    [self data:self.data response:self.response error:self.error completionHandler:^(id response) {
        if (weakSelf.delegate) {
            [weakSelf.delegate didSerialize:response error:nil];
        }
        [weakSelf complete];
    } failure:^(NetworkError *error) {
        if (weakSelf.delegate) {
            [weakSelf.delegate didSerialize:nil error:error];
        }
        [weakSelf complete];
    }];
}

#pragma mark - NetworkOperationDelegate

- (void)didLoadData:(NSData*)data response:(NSURLResponse*)response error:(NSError*)error {
    _data = data;
    _response = response;
    _error = error;
}

#pragma mark - Private

- (void)data:(NSData*)data response:(NSURLResponse*)response error:(NSError*)error completionHandler:(void(^)(id response))success
     failure:(void(^)(NetworkError* error))failure {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    switch (httpResponse.statusCode) {
        case 200: {
            NSDictionary* json;
            if (data) {
                json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            }
                        NSLog(@"%@", json);
            success(json);
            break;
        }
        case 204:{
            NSDictionary* responseHeaders = [(NSHTTPURLResponse *)response allHeaderFields];
            success(responseHeaders);
            break;
        }
        case 400:
        case 401:
        case 403:
        case 404:
        case 405:
        case 406:{
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NetworkError* error = [[NetworkError alloc] initWithText:json[@"error"][@"message"]];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if (error.text == nil) {
                error.text = [NSString stringWithFormat:@"%ld", (long)[httpResponse statusCode]];
                if ((long)[httpResponse statusCode] == 401) {
                    error.text = @"Ошибка авторизации";
                }
                
            }
            error.code = [httpResponse statusCode];
            failure(error);
            break;
        }
        case 500: {
            NetworkError* error = [[NetworkError alloc] initWithText:@"Status Code: 500. Ошибка на сервере. Проверьте параметры и заголовок запроса."];
            failure(error);
            break;
        }
        default:
            if (error) {
                switch (error.code) {
                    case -1001: {
                        NSString* text = @"Превышено допустимое время ожидания ответа от сервера. Возможно, соединение с интернетом прервано. Попробуйте повторить действие позже";
                        NetworkError* mbError = [[NetworkError alloc] initWithText:text];
                        mbError.code = error.code;
                        failure(mbError);
                        break;
                    }
                    case -1009: {
                        NSString* text = @"Отсутствует подключение к сети";
                        NetworkError* mbError = [[NetworkError alloc] initWithText:text];
                        mbError.code = error.code;
                        failure(mbError);
                        break;
                    }
                    default: {
                        NetworkError* mbError = [[NetworkError alloc] initWithText:error.localizedDescription];
                        mbError.code = error.code;
                        failure(mbError);
                        break;
                    }
                }
            }
            break;
    }
}


@end
