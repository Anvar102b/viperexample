//
//  AsyncOperation.h
//  Created by Anvar Basharov on 27.01.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 The base NSOperation class, which provides the asynchronyous capabilities to all of its subclasses.
 */
@interface AsyncOperation : NSOperation

/**
 This method is called in a separate thread after operation start. Should be overriden.
 */
- (void)main;

/** 
 This method sets all operation flags to appropriate values. Should be manually called after operation executes.
 */
- (void)complete;

@end
