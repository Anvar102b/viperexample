//
//  NetworkOperation.h
//  ViperExample
//
//  Created by Anvar Basharov on 27.01.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "AsyncOperation.h"

@protocol NetworkOperationDelegate <NSObject>

- (void)didLoadData:(NSData*)data response:(NSURLResponse*)response error:(NSError*)error;

@end

@interface NetworkOperation : AsyncOperation

@property (nonatomic, weak) id <NetworkOperationDelegate> delegate;

- (instancetype)initWithSession:(NSURLSession *)session request:(NSURLRequest *)request;

@end
