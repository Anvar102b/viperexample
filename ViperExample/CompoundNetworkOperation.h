//
//  CompoundNetworkOperation.h
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "AsyncOperation.h"

@class NetworkError;

@interface CompoundNetworkOperation : AsyncOperation

- (instancetype)initWithSession:(NSURLSession*)session request:(NSURLRequest*)request completion:(void(^)(id response, NetworkError* error))completion;

@property (nonatomic, strong) NSURLRequest* request;

@end
