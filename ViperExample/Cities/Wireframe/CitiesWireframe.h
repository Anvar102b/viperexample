//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CitiesWireframeInput.h"
#import <UIKit/UIKit.h>

@interface CitiesWireframe : NSObject <CitiesWireframeInput>

@end
