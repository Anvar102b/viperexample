//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "CitiesWireframe.h"
//#import "CitiesViewController.h"
//#import "CitiesPresenter.h"
//#import "CitiesInteractor.h"
//#import "CitiesWireframe.h"

@implementation CitiesWireframe

+ (void)presentCitiesModuleFrom:(UIViewController*)fromViewController {
    /* Generating module components
    CitiesViewController *view = [[CitiesViewController alloc] init];
    CitiesPresenter *presenter = [CitiesPresenter new];
    CitiesInteractor *interactor = [CitiesInteractor new];
    CitiesWireframe *wireframe= [CitiesWireframe new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireframe = wireframe;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    //TOODO - New view controller presentation (present, push, pop, .. ) */
}

@end
