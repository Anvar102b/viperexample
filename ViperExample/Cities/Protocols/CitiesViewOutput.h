//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol CitiesViewOutput <NSObject>

- (void)obtainCities;
- (NSInteger)rowsCount;
- (City*)cityAtIndex:(NSInteger)index;

@end
