//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class City;

@protocol CitiesInteractorInput <NSObject>

- (void)loadCities;
- (NSInteger)rowsCount;
- (City*)cityAtIndex:(NSInteger)index;

@end
