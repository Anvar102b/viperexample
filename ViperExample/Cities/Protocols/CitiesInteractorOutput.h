//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CitiesInteractorOutput <NSObject>

- (void)didLoadCities;
- (void)failedLoading:(NSString*)errorText;

@end
