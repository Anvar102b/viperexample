//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CitiesViewInput <NSObject>

- (void)showTableView:(BOOL)show;
- (void)showIndicator:(BOOL)show;
- (void)updateTable;
- (void)showErrorAlertWithText:(NSString*)errorText;

@end
