//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "CitiesInteractor.h"
#import "CitiesPresenter.h"
#import "CitiesService.h"
#import "NetworkError.h"
#import "City.h"

@interface CitiesInteractor ()

@property (nonatomic, strong) NSArray* citiesArray;

@end

@implementation CitiesInteractor

#pragma mark - CitiesInteractorInput

- (void)loadCities {
    __weak __typeof(self)weakSelf = self;
    [self.cityService getCities:^(NSArray *cities) {
        weakSelf.citiesArray = cities;
        [weakSelf.presenter didLoadCities];
    } failure:^(NetworkError *error) {
        [weakSelf.presenter failedLoading:error.text];
    }];
}

- (NSInteger)rowsCount {
    if (self.citiesArray) {
        return self.citiesArray.count;
    }
    return 0;
}

- (City*)cityAtIndex:(NSInteger)index {
    City* city = self.citiesArray[index];
    return city;
}

@end
