//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CitiesInteractorInput.h"

@protocol CitiesInteractorOutput;
@protocol CityServiceProtocol;

@interface CitiesInteractor : NSObject <CitiesInteractorInput>

@property (nonatomic, weak) IBOutlet id <CitiesInteractorOutput> presenter;
@property (nonatomic, strong) IBOutlet id <CityServiceProtocol> cityService;


@end
