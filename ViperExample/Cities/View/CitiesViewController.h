//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitiesViewInput.h"

@protocol CitiesViewOutput;

@interface CitiesViewController : UIViewController <CitiesViewInput>

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView* indicator;
@property (nonatomic, weak) IBOutlet UITableView* tableView;

@property (nonatomic, strong) IBOutlet id <CitiesViewOutput> presenter;

@end
