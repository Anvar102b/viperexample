//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "CitiesViewController.h"
#import "CitiesPresenter.h"
#import "City.h"

@interface CitiesViewController ()

@end

@implementation CitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter obtainCities];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter rowsCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    City* city = [self.presenter cityAtIndex:indexPath.row];
    cell.textLabel.text = city.cityName;
    return cell;
}


#pragma mark - UITableViewDelegate
//...

#pragma mark - CitiesViewInput

- (void)showIndicator:(BOOL)show {
    if (show) {
        [self.indicator startAnimating];
    } else {
        [self.indicator stopAnimating];
    }
}

- (void)showTableView:(BOOL)show {
    [self.tableView setAlpha:(CGFloat)show];
}

- (void)updateTable {
    [self.tableView reloadData];
}

- (void)showErrorAlertWithText:(NSString*)errorText {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:errorText preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
