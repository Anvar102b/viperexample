//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CitiesViewOutput.h"
#import "CitiesInteractorOutput.h"

@protocol CitiesViewInput;
@protocol CitiesInteractorInput;
@protocol CitiesWireframeInput;

@interface CitiesPresenter : NSObject <CitiesViewOutput, CitiesInteractorOutput>

@property (nonatomic, weak) IBOutlet id <CitiesViewInput> view;
@property (nonatomic, strong) IBOutlet id <CitiesInteractorInput> interactor;
@property (nonatomic, strong) IBOutlet id <CitiesWireframeInput> wireframe;

@end
