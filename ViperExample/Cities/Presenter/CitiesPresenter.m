//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "CitiesPresenter.h"
#import "CitiesWireframe.h"
#import "CitiesInteractor.h"
#import "CitiesViewController.h"
#import "City.h"

@implementation CitiesPresenter

#pragma mark - CitiesViewOutput

- (void)obtainCities {
    [self.view showTableView:NO];
    [self.view showIndicator:YES];
    [self.interactor loadCities];
    
}

- (NSInteger)rowsCount {
    return [self.interactor rowsCount];
}

- (City*)cityAtIndex:(NSInteger)index {
    return [self.interactor cityAtIndex:index];
}

#pragma mark - CitiesInteractorOutput

- (void)didLoadCities {
    [self.view showIndicator:NO];
    [self.view showTableView:YES];
    [self.view updateTable];
}

- (void)failedLoading:(NSString *)errorText {
    [self.view showIndicator:NO];
    [self.view showErrorAlertWithText:errorText];
}

@end
