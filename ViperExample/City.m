//
//  City.m
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "City.h"

@implementation City

- (instancetype)initWithResponse:(NSDictionary*)response
{
    self = [super init];
    if (self) {
        self.cityName = response[@"Name"];
    }
    return self;
}

@end
