//
//  OperationSchedulerProtocol.h
//  ViperExample
//
//  Created by Anvar Basharov on 02.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OperationSchedulerProtocol <NSObject>

- (void)addOperation:(NSOperation *)operation;
- (void)cancelAllOperations;

@end
