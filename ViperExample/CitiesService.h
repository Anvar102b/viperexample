//
//  CitiesService.h
//  ViperExample
//
//  Created by Anvar Basharov on 01.02.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityServiceProtocol.h"

@class CompoundNetworkOperation;
@protocol OperationSchedulerProtocol;

@interface CitiesService : NSObject <CityServiceProtocol>

@property (strong, nonatomic) IBOutlet id <OperationSchedulerProtocol> operationScheduler;

@end
